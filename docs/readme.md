# Readme

Markdown to HTML

https://github.com/mixu/markdown-styles

# Install

Install nodeJs `5.+.+` version

Install dependencies

`$ npm install`

# Usage

Move `.md` files to `./input` folder

Run generation and web server on `3000` port.

`$ gulp`

Or set different `input` path

`$ gulp watch --input ./input`
