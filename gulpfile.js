var gulp = require('gulp');
var fs = require('fs-extra')
var connect = require('gulp-connect');

var mds = require('markdown-styles'),
    path = require('path');
var util = require('util');

// get args
var inputPath = 'input';

var cliArg = process.argv.slice(2);
for (var i = 0; i < cliArg.length; i++) {
    if (cliArg[i] === '--input') {
        inputPath = cliArg[i + 1];
        break;
    }
}




gulp.task('default', ['clear', 'generate', 'connect', 'watch']);

gulp.task('watch', function () {
    gulp.watch([inputPath + '/*.md', inputPath + '/**/*.md'], ['clear', 'generate']);
});

gulp.task('generate', function (callBack) {


    var getFilesRecursive = function(folder) {
        var fileContents = fs.readdirSync(folder),
            fileTree = [],
            stats;
        var folderRel;

        fileContents.forEach(function (fileName) {
            // TODO ignore files
            if (!['.gitignore', 'img'].includes(fileName)) {
                stats = fs.lstatSync(folder + '/' + fileName);


                folderRel = folder.replace(inputPath, '');

                if (stats.isDirectory()) {
                    fileTree.push({
                        name: fileName,
                        folder: folderRel,
                        type: 'folder',
                        children: getFilesRecursive(folder + '/' + fileName)
                    });
                } else {
                    // Only MD
                    if (typeof(fileName) === 'string' && fileName.indexOf('.md') > 0) {
                        fileTree.push({
                            name: fileName,
                            folder: folderRel,
                            type: 'file'
                        });
                    }
                }
            }
        });

        return fileTree;
    };

    var menuStructure = getFilesRecursive(inputPath);

    mds.render(mds.resolveArgs({
        input: path.normalize(inputPath),
        output: path.normalize(process.cwd() + '/output'),
        layout: path.normalize(process.cwd() + '/my-layout'),
        'header-links': true,
        meta: {
            "*": {
                "menu": menuStructure
            }
        }
    }), function() {
        callBack();
        console.log('generation done!');
    });
});


gulp.task('connect', function() {
    connect.server({
        root: 'output',
        port: 3000
    });
});


gulp.task('clear', function (callBack) {
    fs.removeSync(process.cwd() + '/output/*', callBack);
});

