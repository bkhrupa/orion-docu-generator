var Handlebars = require('handlebars');
module.exports = function(){
    if (!this.url) {
        return Handlebars.Utils.escapeExpression(this.body);
    }

    return new Handlebars.SafeString("<a href='" + Handlebars.Utils.escapeExpression(this.url) + "'>" + Handlebars.Utils.escapeExpression(this.body) + "</a>");
};
