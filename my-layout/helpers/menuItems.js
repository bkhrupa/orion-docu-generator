var Handlebars = require('handlebars');



function menuItem(item) {
    var html = '';
    var name;

    if (item.type === 'folder') {
        html += '<li>' + item.name + '</li>';
    }
    else if(item.type === 'file') {
        name = item.name.replace('.md', '.html');

        html += '<li><a href="' +item.folder + '/' + name +'">' + item.name.replace(/\.md$/, '') + '</a></li>';
    }

    if (item.children) {
        html += '<ul>';
        item.children.forEach(function (childItem) {
            html += menuItem(childItem);
        });
        html += '</ul>';
    }

    return html;
}

module.exports = function(){
    return menuItem(this);

};
